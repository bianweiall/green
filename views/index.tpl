<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <title>管理系统首页</title>
    {{.IE}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/static/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/static/js/lib/bui/css/bs3/dpl-min.css" rel="stylesheet">
    <link href="/static/js/lib/bui/css/bs3/bui-min.css" rel="stylesheet">
    <link href="/static/css/zTreeStyle.css" rel="stylesheet">
    <link href="/static/css/green.css" rel="stylesheet"></head>

<body>
    <header>
        <div class="navbar navbar-default navbar-static-top my-navbar-1" role="navigation">
            <div class="container">
                <div class="navbar-header my-navbar-1-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">绿诚ERP</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul id="my-navbar-1-nav" class="nav navbar-nav my-navbar-1-nav">
                        <li class="my-navbar-1-nav-li-active">
                            <a class="a-level-1" href="/green">首页</a>
                        </li>
                        <li>
                            <a href="#">客户</a>
                        </li>
                        <!--product start-->
                        <li class="dropdown my-dropdown-1">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                产品 <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="product-admin">
                                    <a href="#">产品管理</a>
                                </li>
                                <li>
                                    <a href="#">统计</a>
                                </li>
                            </ul>
                        </li>
                        <!--product end-->
                        <!--warehouse start-->
                        <li class="dropdown my-dropdown-1">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                库存 <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="warehouse-admin">
                                    <a href="#">库存管理</a>
                                </li>
                                <li>
                                    <a href="#">入库单</a>
                                </li>
                                <li>
                                    <a href="#">出库单</a>
                                </li>
                                <li>
                                    <a href="#">采购单</a>
                                </li>
                                <li>
                                    <a href="#">统计</a>
                                </li>
                            </ul>
                        </li>
                        <!--warehouse end--> </ul>
                    <ul class="nav navbar-nav navbar-right  my-navbar-1-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Admin，你好！
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">详细信息</a>
                                </li>
                                <li>
                                    <a href="#">退出登录</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="navbar navbar-default my-navbar-2" role="navigation">
            <div class="container">
                <div id="navbar-2" class="navbar-collapse collapse my-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="/green">管理首页</a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse --> </div>
        </div>
    </header>
    <article class="container">
        <div class="main">
            <div id="main-body" class="main-body"></div>
        </div>
    </article>
    <footer>
        <div class="container">
            <p>Copyright ©2013 GREEN 系统</p>
        </div>
    </footer>

    <!--类库和插件-->
    <script src="/static/js/lib/jquery-1.11.0.min.js"></script>
    <script src="/static/js/lib/bootstrap.min.js"></script>
    <script src="/static/js/lib/bui/seed.js"></script>
    <script src="/static/js/lib/json2.js"></script>
    <script src="/static/js/lib/jquery.ztree.all-3.5.min.js"></script>

    <!--<script src="/static/js/green-ui.js"></script>
-->
<!--GREEN-->
<script src="/static/js/green.js"></script>
<script src="/static/js/warehouse.js"></script>
<script src="/static/js/data.js"></script>
<!--
  <script src="/static/js/utils.js"></script>
<script src="/static/js/ztreeCustom.js"></script>
<script src="/static/js/goodsAdmin.js"></script>
-->
</body>

</html>