<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8">
  <title>管理系统首页</title>
  {{.IE}}
  <link href="/static/css/zTreeStyle.css" rel="stylesheet">
  <link href="/static/css/green-ui.css" rel="stylesheet">
  <link href="/static/css/green.css" rel="stylesheet">

  <script src="/static/js/lib/jquery-1.10.2.min.js"></script>
  <script src="/static/js/lib/json2.js"></script>
  <script src="/static/js/lib/jquery.ztree.min.js"></script>
  <script src="/static/js/green-ui.js"></script>
  <script src="/static/js/green.js"></script>
</head>
<body>
  <div id="container">
    <header>
      <span>Green</span>
      <p>欢迎 ，bianweiall</p>
      <a href="#" id="logout">退出</a>
    </header>
    <div class="clear"></div>
    <div id="main">
      <!--左侧导航开始-->
      <nav>
        <ul>
          <li class="active">
            <a href="/green">后台首页</a>
          </li>
          <li>
            <a href="/green/category/list">分类管理</a>
          </li>
          <li id="cangkuguanli">
            <a href="#">仓库管理</a>
          </li>
          <li>IT新闻</li>
        </ul>
      </nav>
      <!--左侧导航结束-->

      <!--中间TREE导航开始-->
      <div id="treeBox">
        <p>树状导航</p>
        <ul id="tree" class="ztree"></ul>
      </div>
      <!--中间TREE导航结束-->

      <!--中间TREE右键菜单开始-->
      <div id="rMenu">
        <ul>
          <li id="m_add">增加</li>
          <li id="m_rename">重命名</li>
          <li id="m_del">删除</li>
        </ul>
      </div>
      <!--中间TREE右键菜单结束-->

      <!--右边详细信息开始-->
      <article>
        <p>后台首页-详细信息</p>
        <div id="content"></div>
      </article>
      <!--右边详细信息结束--> </div>
    <div class="clear"></div>

    <footer>
      <p>Copyright ©2013 GREEN 系统</p>
    </footer>
  </div>

</body>
</html>