package models

import (
	"github.com/astaxie/beego/orm"
	//o "myapp/web/green/models/orm"
)

//CRUD操作接口
// type DBHelper interface {
// 	Create(orm.Ormer) (int64, error)
// 	ReadOrCreate(orm.Ormer, string) (bool, int64, error)
// 	Read(orm.Ormer, ...string) (DBHelper, error)
// 	//ReadOrCreate(orm.Ormer, string, ...string) (bool, int64, error)

// 	Update(orm.Ormer, ...string) error
// 	UpdateMulti(orm.Ormer, orm.Params, ...interface{}) error
// 	Delete(orm.Ormer) error
// 	List(orm.Ormer) ([]orm.Params, error)
// 	GetRawSeter(orm.Ormer, string, ...interface{}) orm.RawSeter
// }

type Creater interface {
	Create(orm.Ormer) (int64, error)
}

type ReadOrCreater interface {
	ReadOrCreate(orm.Ormer, string) (bool, int64, error)
}

type Reader interface {
	Read(orm.Ormer, ...string) (Reader, error)
}

type Updater interface {
	Update(orm.Ormer, ...string) error
}

type MultiUpdater interface {
	MultiUpdate(orm.Ormer, orm.Params, ...interface{}) error
}

type Deleter interface {
	Delete(orm.Ormer) error
}

type MultiDeleter interface {
	MultiDelete(orm.Ormer, ...interface{}) error
}

type Lister interface {
	List(orm.Ormer) ([]orm.Params, error)
}

// type GetRawSeter interface {
// 	GetRawSeter(orm.Ormer, string, ...interface{}) orm.RawSeter
// }

func NewOrmer() orm.Ormer {
	return orm.NewOrm()
}
