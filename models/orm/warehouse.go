package orm

import (
	"github.com/astaxie/beego/orm"
	"myapp/web/green/models"
)

//仓库
type Warehouse struct {
	Id    int    `orm:"column(_id);pk;auto"`
	Name  string `orm:"column(_name)"`
	Level int    `orm:"column(_level)"`
	Fid   int    `orm:"column(_fid)"`
}

func (this *Warehouse) List(o orm.Ormer) ([]orm.Params, error) {
	//o := orm.NewOrm()
	var list []orm.Params
	_, err := o.QueryTable("_warehouse").Values(&list)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (this *Warehouse) Create(o orm.Ormer) (int64, error) {
	//o := orm.NewOrm()
	id, err := o.Insert(this)
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (this *Warehouse) ReadOrCreate(o orm.Ormer, str string) (bool, int64, error) {
	created, id, err := o.ReadOrCreate(this, str)
	return created, id, err
}

func (this *Warehouse) Read(o orm.Ormer, fields ...string) (models.Reader, error) {
	//o := orm.NewOrm()
	err := o.Read(this, fields...)
	if err != nil {
		return nil, err
	}
	return this, nil
}

// func (this *Warehouse) ReadOrCreate(o orm.Ormer, str string, fields ...string) (bool, int64, error) {
// 	//o := orm.NewOrm()
// 	created, id, err := o.ReadOrCreate(this, str, fields...)
// 	if err != nil {
// 		return false, -1, err
// 	}
// 	return created, id, nil
// }

//通过数据库主键字段更新，可指定更新字段，参数为struct字段的字符串
func (this *Warehouse) Update(o orm.Ormer, fields ...string) error {
	//o := orm.NewOrm()
	_, err := o.Update(this, fields...)
	if err != nil {
		return err
	}
	return nil
}

//更新多条记录
func (this *Warehouse) MultiUpdate(o orm.Ormer, params orm.Params, args ...interface{}) error {
	_, err := o.QueryTable("_warehouse").Filter("_id__in", args).Update(params)
	//_, err = o.Update(this, fields...)
	if err != nil {
		return err
	}
	return nil
}

//通过数据库主键字段删除
func (this *Warehouse) Delete(o orm.Ormer) error {
	//o := orm.NewOrm()
	_, err := o.Delete(this)
	if err != nil {
		return err
	}
	return nil
}

func (this *Warehouse) GetRawSeter(o orm.Ormer, sql string, args ...interface{}) orm.RawSeter {
	return o.Raw(sql, args...)
}
