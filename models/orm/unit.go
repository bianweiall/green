package orm

import (
	"github.com/astaxie/beego/orm"
)

//数量和价格单位
type Unit struct {
	Id     int    `orm:"column(_id);pk;auto"` //单位ID
	Name   string `orm:"column(_name)"`       //名称
	Status int    `orm:"column(_status)"`     //状态 0=数量单位 1=价格单位
}

func (this *Unit) List() ([]orm.Params, error) {
	o := orm.NewOrm()
	var list []orm.Params
	_, err := o.QueryTable("_warehouse").Values(&list)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (this *Unit) Create() (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(this)
	if err != nil {
		return -1, err
	}
	return id, nil
}

//通过数据库主键字段更新，可指定更新字段，参数为struct字段的字符串
func (this *Unit) Update(fields ...string) error {
	o := orm.NewOrm()
	_, err := o.Update(this, fields...)
	if err != nil {
		return err
	}
	return nil
}

//通过数据库主键字段删除
func (this *Unit) Delete() error {
	o := orm.NewOrm()
	_, err := o.Delete(this)
	if err != nil {
		return err
	}
	return nil
}

func (this *Unit) GetRawSeter(sql string, args ...interface{}) orm.RawSeter {
	return orm.NewOrm().Raw(sql, args...)
}
