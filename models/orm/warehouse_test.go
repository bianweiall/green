package orm

//import (
//	"fmt"
//	"github.com/astaxie/beego/orm"
//	"testing"
//)

//func TestList(t *testing.T) {
//	var list WarehouseList
//	ls, err := list.List()
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//	fmt.Println("ls: ", ls)
//}

//func TestCreate(t *testing.T) {
//	var w Warehouse
//	w.Fid = 0
//	w.Level = 0
//	w.Name = "我的标题"
//	id, err := w.Create()
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//	fmt.Println("id: ", id)
//}

//func TestUpdate(t *testing.T) {
//	var w Warehouse
//	w.Id = 131
//	w.Fid = 2
//	w.Level = 2
//	w.Name = "我的标题4"
//	err := w.Update("Name", "Fid", "Level")
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//}

//func TestUpdateBySql(t *testing.T) {
//	var w Warehouse
//	w.Id = 131
//	w.Fid = 2
//	w.Level = 2
//	w.Name = "我的标题4"
//	sql := "UPDATE _warehouse SET _fid = ? WHERE _name = ?"
//	err := w.UpdateBySql(sql, 20, "我的标题10")
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//}

//func TestDelete(t *testing.T) {
//	var w Warehouse
//	//w.Id = 131
//	w.Name = "我的标题"
//	err := w.Delete()
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//}

//func TestDeleteBySql(t *testing.T) {
//	var w Warehouse
//	w.Id = 131
//	w.Fid = 2
//	w.Level = 2
//	w.Name = "我的标题4"
//	sql := "DELETE FROM _warehouse WHERE _name = ?"
//	err := w.UpdateBySql(sql, "我的标题")
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//}

//func TestExecSql(t *testing.T) {
//	var w Warehouse
//	w.Id = 131
//	w.Fid = 2
//	w.Level = 2
//	w.Name = "我的标题4"
//	//_, err := w.ExecSql("UPDATE _warehouse SET _fid = ? WHERE _name = ?", 20, "我的标题")
//	_, err := w.ExecSql("DELETE FROM _warehouse WHERE _name = ?", "我的标题")
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//}

//func TestGetRawSeter(t *testing.T) {
//	var ws []orm.Params
//	w := Warehouse{}
//	mun, err := w.GetRawSeter("SELECT * FROM _warehouse WHERE _id > ?", 125).Values(&ws)
//	if err != nil {
//		fmt.Println("err: ", err)
//	}
//	fmt.Println("mun: ", mun)
//	fmt.Println("ws: ", ws)
//}
