package orm

// import (
// 	"github.com/astaxie/beego/orm"
// )

// //分类
// type Category struct {
// 	Id    int    `orm:"column(_id);pk;auto"`
// 	Name  string `orm:"column(_name)"`
// 	Level int    `orm:"column(_level)"`
// 	Fid   int    `orm:"column(_fid)"`
// }

// func (this *Category) List() ([]orm.Params, error) {
// 	o := orm.NewOrm()
// 	var list []orm.Params
// 	_, err := o.QueryTable("_warehouse").Values(&list)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return list, nil
// }

// //创建并且返回刚插入的这条记录的Id
// func (this *Category) Create() (int64, error) {
// 	o := orm.NewOrm()
// 	id, err := o.Insert(this)
// 	if err != nil {
// 		return -1, err
// 	}
// 	return id, nil
// }

// //通过数据库主键字段更新，可指定更新字段，参数为struct字段的字符串
// func (this *Category) Update(fields ...string) error {
// 	o := orm.NewOrm()
// 	_, err := o.Update(this, fields...)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// //通过数据库主键字段删除
// func (this *Category) Delete() error {
// 	o := orm.NewOrm()
// 	_, err := o.Delete(this)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// func (this *Category) GetRawSeter(sql string, args ...interface{}) orm.RawSeter {
// 	return orm.NewOrm().Raw(sql, args...)
// }
