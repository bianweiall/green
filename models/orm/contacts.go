package orm

import (
	"github.com/astaxie/beego/orm"
)

//联系人
type Contacts struct {
	Company
	Id int `orm:"column(_id);pk;auto"` //供应商ID
	//Companyid int    `orm:"column(_name)"`       //公司ID
	Name    string `orm:"column(_name)"`    //名称
	Phone   string `orm:"column(_phone)"`   //电话
	Fax     string `orm:"column(_fax)"`     //传真
	Email   string `orm:"column(_email)"`   //邮箱
	Address string `orm:"column(_address)"` //地址
}

func (this *Contacts) List() ([]orm.Params, error) {
	o := orm.NewOrm()
	var list []orm.Params
	_, err := o.QueryTable("_warehouse").Values(&list)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (this *Contacts) Create() (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(this)
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (this *Contacts) ReadOrCreate(o orm.Ormer, str string) (bool, int64, error) {
	created, id, err := o.ReadOrCreate(this, str)
	return created, id, err
}

//通过数据库主键字段更新，可指定更新字段，参数为struct字段的字符串
func (this *Contacts) Update(fields ...string) error {
	o := orm.NewOrm()
	_, err := o.Update(this, fields...)
	if err != nil {
		return err
	}
	return nil
}

//更新多条记录
func (this *Contacts) MultiUpdate(o orm.Ormer, params orm.Params, args ...interface{}) error {
	_, err := o.QueryTable("_goods").Filter("_id__in", args).Update(params)
	//_, err = o.Update(this, fields...)
	if err != nil {
		return err
	}
	return nil
}

//通过数据库主键字段删除
func (this *Contacts) Delete() error {
	o := orm.NewOrm()
	_, err := o.Delete(this)
	if err != nil {
		return err
	}
	return nil
}

func (this *Contacts) GetRawSeter(sql string, args ...interface{}) orm.RawSeter {
	return orm.NewOrm().Raw(sql, args...)
}
