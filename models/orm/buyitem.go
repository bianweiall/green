package orm

import (
	"github.com/astaxie/beego/orm"
	"myapp/web/green/models"
)

//物品采购单
type BuyItem struct {
	Id         int     `orm:"column(_id);pk;auto"`                        //详单ID
	Supplier   string  `orm:"column(_supplier)"`                          //联系人
	Quantity   float64 `orm:"column(_quantity);digits(25);decimals(5)"`   //数量
	Unitprice  float64 `orm:"column(_unitprice);digits(25);decimals(5)"`  //单价
	Totalprice float64 `orm:"column(_totalprice);digits(25);decimals(5)"` //总价
	Purpose    string  `orm:"column(_purpose);null"`                      //用途
	Other      string  `orm:"column(_other);null"`                        //其他备注
	Createuser string  `orm:"column(_createuser)"`                        //申请人
	Ratifyuser string  `orm:"column(_ratifyuser)"`                        //批准人
	Createtime string  `orm:"column(_createtime)"`                        //申请时间
	Ratifytime string  `orm:"column(_ratifytime)"`                        //批准时间
	Status     int     `orm:"column(_status)"`                            //状态 -1=未审核 0=已审核 1=in

	Goodsid int64 `orm:"column(_goodsid)"` //物品ID

	// Goods *Goods `orm:"rel(one)"`
	Goods *Goods `orm:"-"`

	//Orm orm.Ormer `orm:"-"`
}

//自定义数据库表名
func (this *BuyItem) TableName() string {
	return "buyitem"
}

func (this *BuyItem) List(o orm.Ormer) ([]orm.Params, error) {
	//o := orm.NewOrm()
	//o := this.Orm
	var list []orm.Params
	_, err := o.QueryTable("_warehouse").Values(&list)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (this *BuyItem) Create(o orm.Ormer) (int64, error) {
	//o := orm.NewOrm()
	//o := this.Orm
	id, err := o.Insert(this)
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (this *BuyItem) ReadOrCreate(o orm.Ormer, str string) (bool, int64, error) {
	created, id, err := o.ReadOrCreate(this, str)
	return created, id, err
}

func (this *BuyItem) Read(o orm.Ormer, fields ...string) (models.Reader, error) {
	//o := orm.NewOrm()
	//o := this.Orm
	err := o.Read(this, fields...)
	if err != nil {
		return nil, err
	}
	return this, nil
}

// func (this *BuyItem) ReadOrCreate(o orm.Ormer, str string, fields ...string) (bool, int64, error) {
// 	//o := orm.NewOrm()
// 	//o := this.Orm
// 	created, id, err := o.ReadOrCreate(this, str, fields...)
// 	if err != nil {
// 		return false, -1, err
// 	}
// 	return created, id, nil
// }

//通过数据库主键字段更新，可指定更新字段，参数为struct字段的字符串
func (this *BuyItem) Update(o orm.Ormer, fields ...string) error {
	//o := orm.NewOrm()
	//o := this.Orm
	_, err := o.Update(this, fields...)
	if err != nil {
		return err
	}
	return nil
}

//更新多条记录
func (this *BuyItem) MultiUpdate(o orm.Ormer, params orm.Params, args ...interface{}) error {
	_, err := o.QueryTable("_goods").Filter("_id__in", args).Update(params)
	//_, err = o.Update(this, fields...)
	if err != nil {
		return err
	}
	return nil
}

//通过数据库主键字段删除
func (this *BuyItem) Delete(o orm.Ormer) error {
	//o := orm.NewOrm()
	//o := this.Orm
	_, err := o.Delete(this)
	if err != nil {
		return err
	}
	return nil
}

//返回orm.RawSeter对象
func (this *BuyItem) GetRawSeter(o orm.Ormer, sql string, args ...interface{}) orm.RawSeter {
	//o := orm.NewOrm()
	//o := this.Orm
	return o.Raw(sql, args...)
}
