package main

import (
	//"fmt"
	"github.com/astaxie/beego"
	//"github.com/astaxie/beego/orm"
	c "myapp/web/green/controllers"
)

func main() {
	// //数据库建表

	// // 数据库别名
	// name := "default"

	// // drop table 后再建表
	// force := true

	// // 打印执行过程
	// verbose := true

	// // 遇到错误立即返回
	// err := orm.RunSyncdb(name, force, verbose)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	//orm.Debug = true

	//static
	beego.SetStaticPath("/html", "html")
	beego.SetStaticPath("/fonts", "fonts")
	beego.SetStaticPath("/htc", "htc")
	beego.SetStaticPath("/temp", "temp")

	//index

	beego.Router("/green", &c.IndexController{})

	////user
	//beego.Router("/green/user/create", &c.UserCreate{})
	//beego.Router("/green/user/login", &c.UserLogin{})
	//beego.Router("/green/user/logout", &c.UserLogout{})

	////category
	//beego.Router("/green/category/create", &c.CategoryCreate{})
	//beego.Router("/green/category/update", &c.CategoryUpdate{})
	//beego.Router("/green/category/delete/:id:int", &c.CategoryDelete{})
	//beego.Router("/green/category/list", &c.CategoryList{})

	//warehouse
	beego.Router("/green/warehouse/create", &c.WarehouseCreate{})
	beego.Router("/green/warehouse/update", &c.WarehouseUpdate{})
	beego.Router("/green/warehouse/delete/:id:int", &c.WarehouseDelete{})
	beego.Router("/green/warehouse/list", &c.WarehouseList{})

	//goods
	beego.Router("/green/goods/list/:warehouseid:int/:limit:int/:offset:int", &c.GoodsList{})
	beego.Router("/green/goods/create", &c.GoodsCreate{})
	beego.Router("/green/goods/update", &c.GoodsUpdate{})
	beego.Router("/green/goods/multiupdate", &c.MultiGoodsUpdate{})
	beego.Router("/green/goods/delete/:id:int", &c.GoodsDelete{})
	beego.Router("/green/goods/multidelete", &c.MultiGoodsDelete{})
	beego.Router("/green/goods/count", &c.GoodsCount{})

	////product
	//beego.Router("/green/product/create", &c.ProductCreate{})
	//beego.Router("/green/product/list", &c.ProductList{})

	//BuyItem
	//beego.Router("/green/buyitem/create", &c.BuyItemCreate{})

	beego.Run()
}
