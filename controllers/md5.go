package controllers

import (
	"crypto/md5"
	"fmt"
	"io"
)

func Password2Md5(password string) string {
	h1 := md5.New()
	io.WriteString(h1, password)

	pwmd5 := fmt.Sprintf("%x", h1.Sum(nil))

	salt1 := "@#$%"
	salt2 := "^&*()"
	salt3 := "14ab3226eafe602446843221ca21aae5"

	io.WriteString(h1, salt1)
	io.WriteString(h1, password)
	io.WriteString(h1, salt2)
	io.WriteString(h1, pwmd5)
	io.WriteString(h1, salt3)

	pw := fmt.Sprintf("%x", h1.Sum(nil))

	return pw
}

func Username2Md5(username string) string {
	h0 := md5.New()
	io.WriteString(h0, username)

	unmd5 := fmt.Sprintf("%x", h0.Sum(nil))

	salt1 := "@#$%"
	salt2 := "^&*()"
	salt3 := "14ab3226eafe602446843221ca21aae5"

	io.WriteString(h0, salt1)
	io.WriteString(h0, username)
	io.WriteString(h0, salt2)
	io.WriteString(h0, unmd5)
	io.WriteString(h0, salt3)

	un := fmt.Sprintf("%x", h0.Sum(nil))

	return un
}
