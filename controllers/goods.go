package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	borm "github.com/astaxie/beego/orm"
	"myapp/web/green/models"
	"myapp/web/green/models/orm"
	"strconv"
)

//列表
type GoodsList struct {
	beego.Controller
}

func (this *GoodsList) Get() {
	id, _ := strconv.Atoi(this.Ctx.Input.Param(":warehouseid"))
	limit, _ := strconv.Atoi(this.Ctx.Input.Param(":limit"))
	offset, _ := strconv.Atoi(this.Ctx.Input.Param(":offset"))
	//id2int, _ := strconv.Atoi(id)
	fmt.Println("id: ", id)
	fmt.Println("limit: ", limit)
	fmt.Println("offset: ", offset)
	//var obj models.Lister = &orm.Goods{}
	ormer := models.NewOrmer()
	var list []borm.Params
	rs := ormer.Raw("SELECT * FROM _goods WHERE _warehouseid=? ORDER BY _id DESC LIMIT ? OFFSET ?", id, limit, offset)
	//rs := obj.GetRawSeter(ormer, "SELECT * FROM _goods WHERE _warehouseid=? ORDER BY _id DESC", id2int)
	_, err := rs.Values(&list)
	//_, err := rs.RowsToStruct(&orm.Goods{}, "_number", "_name")
	fmt.Println("list: ", list)
	// if err != nil {
	// 	this.Data["json"] = &HttpMessage{Status: 1}
	// } else {
	// 	this.Data["json"] = &list
	// }
	if err == nil {
		this.Data["json"] = &list
	}
	this.ServeJson()
}

//创建
type GoodsCreate struct {
	beego.Controller
}

func (this *GoodsCreate) Post() {
	var goods orm.Goods
	body := this.Ctx.Input.RequestBody
	json.Unmarshal(body, &goods)
	var obj models.ReadOrCreater = &goods
	ormer := models.NewOrmer()
	// id, err := obj.Create(ormer)
	created, id, err := obj.ReadOrCreate(ormer, "Number")
	if err != nil {
		this.Data["json"] = &HttpMessage{Status: 1}
	} else {
		if created { //新创建的
			this.Data["json"] = &HttpMessage{Status: 0, Data: id}
		} else {
			this.Data["json"] = &HttpMessage{Status: -1}
		}
	}
	this.ServeJson()
	fmt.Println("goods create err: ", err)
}

//更新1条记录
type GoodsUpdate struct {
	beego.Controller
}

func (this *GoodsUpdate) Post() {
	var goods orm.Goods
	body := this.Ctx.Input.RequestBody
	json.Unmarshal(body, &goods)
	var obj models.Updater = &goods
	ormer := models.NewOrmer()
	err := obj.Update(ormer)
	if err != nil {
		this.Data["json"] = &HttpMessage{Status: 1}
	} else {
		this.Data["json"] = &HttpMessage{Status: 0}
	}
	this.ServeJson()
	fmt.Println("goods update err: ", err)
}

//更新多条记录
type MultiGoodsUpdate struct {
	beego.Controller
}

func (this *MultiGoodsUpdate) Post() {
	var goods orm.Goods
	var myMap borm.Params
	body := this.Ctx.Input.RequestBody
	json.Unmarshal(body, &myMap)
	idList := myMap["_modify_id_list"]
	delete(myMap, "_modify_id_list")
	if myMap["_warehouseid"] != nil {
		if value, ok := myMap["_warehouseid"].(float64); ok {
			myMap["_warehouseid"] = int(value)
		}
	}
	fmt.Println("myMap: ", myMap)
	fmt.Println("idList: ", idList)

	var obj models.MultiUpdater = &goods
	ormer := models.NewOrmer()
	err := obj.MultiUpdate(ormer, myMap, idList)
	fmt.Println("MultiGoodsUpdate err: ", err)
	if err != nil {
		this.Data["json"] = &HttpMessage{Status: 1}
	} else {
		this.Data["json"] = &HttpMessage{Status: 0}
	}
	this.ServeJson()
}

// //更新Warehouseid
// type MultiGoodsWarehouseidUpdate struct {
// 	beego.Controller
// }

// func (this *GoodsWarehouseidUpdate) Post() {
// 	var goods orm.Goods
// 	var myMap borm.Params
// 	body := this.Ctx.Input.RequestBody
// 	json.Unmarshal(body, &myMap)
// 	idList := myMap["_modify_id_list"]
// 	delete(myMap, "_modify_id_list")
// 	if myMap["_warehouseid"] != nil {
// 		if value, ok := myMap["_warehouseid"].(float64); ok {
// 			myMap["_warehouseid"] = int(value)
// 		}
// 	}
// 	fmt.Println("myMap: ", myMap)
// 	fmt.Println("idList: ", idList)

// 	var obj models.MultiUpdater = &goods
// 	ormer := models.NewOrmer()
// 	err := obj.MultiUpdate(ormer, myMap, idList)
// 	fmt.Println("MultiGoodsUpdate err: ", err)
// 	if err != nil {
// 		this.Data["json"] = &HttpMessage{Status: 1}
// 	} else {
// 		this.Data["json"] = &HttpMessage{Status: 0}
// 	}
// 	this.ServeJson()
// }

//统计
type GoodsCount struct {
	beego.Controller
}

func (this *GoodsCount) Post() {
	var goods orm.Goods
	body := this.Ctx.Input.RequestBody
	json.Unmarshal(body, &goods)
	ormer := models.NewOrmer()
	num, err := ormer.QueryTable("_goods").Filter("_warehouseid", goods.Warehouseid).Count()
	if err == nil {
		this.Data["json"] = &HttpMessage{ServerError: false, Data: num}
		// if num == 0 { //数据库没有记录
		// 	this.Data["json"] = &HttpMessage{ServerError: false, Data: ""}
		// } else { //数据库有记录
		// 	this.Data["json"] = &HttpMessage{ServerError: false, Data: num}
		// }
	} else {
		this.Data["json"] = &HttpMessage{ServerError: true}
	}
	// if err == nil && num == 0 {
	// 	this.Data["json"] = &HttpMessage{Status: 0}
	// } else {
	// 	this.Data["json"] = &HttpMessage{Status: 1}
	// }
	this.ServeJson()
	fmt.Println("goods count err: ", err)
}

//删除一条记录
type GoodsDelete struct {
	beego.Controller
}

func (this *GoodsDelete) Get() {
	id := this.Ctx.Input.Param(":id")
	//id2int, _ := strconv.Atoi(id)
	id2int, _ := strconv.ParseInt(id, 10, 64)
	var obj models.Deleter = &orm.Goods{Id: id2int}
	ormer := models.NewOrmer()
	err := obj.Delete(ormer)
	if err != nil {
		this.Data["json"] = &HttpMessage{Status: 1}
	} else {
		this.Data["json"] = &HttpMessage{Status: 0}
	}
	this.ServeJson()
	fmt.Println("goods delete err: ", err)
}

//删除多条记录
type MultiGoodsDelete struct {
	beego.Controller
}

func (this *MultiGoodsDelete) Post() {
	var idList []interface{}
	body := this.Ctx.Input.RequestBody
	json.Unmarshal(body, &idList)
	//fmt.Println("delete goods id: ", idList)
	var obj models.MultiDeleter = &orm.Goods{}
	ormer := models.NewOrmer()
	err := obj.MultiDelete(ormer, idList)
	if err != nil {
		this.Data["json"] = &HttpMessage{Status: 1}
	} else {
		this.Data["json"] = &HttpMessage{Status: 0}
	}
	this.ServeJson()
	fmt.Println("goods delete err: ", err)
}
