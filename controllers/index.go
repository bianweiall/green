package controllers

import (
	"github.com/astaxie/beego"
)

type IndexController struct {
	beego.Controller
}

func (this *IndexController) Get() {
	this.Data["IE"] = beego.Str2html(`<!--[if lt IE 9]><script src="/static/js/lib/html5shiv.js"></script><script src="/static/js/lib/respond.min.js"></script><![endif]-->`)
	this.TplNames = "index.tpl"
	err := this.Render()
	if err != nil {
		this.Ctx.WriteString("网站打开出错！")
	}
}
