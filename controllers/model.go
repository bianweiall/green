package controllers

//返回客户端消息
type HttpMessage struct {
	Status      int //0=success,1=error,其他数字代表自定义
	Data        interface{}
	ServerError bool
}
