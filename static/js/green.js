$(document).ready(function() {
    //库存管理
    warehouseAdmin();
    //退出系统
    $("#logout").click(function() {
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/green/user/logout",
            data: {},
            //async : false,
            success: function(data) {
                $.each(data, function(k, v) {
                    if (v.Key == "successMessage") {
                        alert("退出成功");
                    }
                });
            }
        });
    });

});

function openCreateGoodsDialog(obj) {
    $("#create-goods-dialog").dialog("close");
    var $createGoodsDialog = $("<div id=\"create-goods-dialog\"></div>");
    $("body").append($createGoodsDialog);
    $createGoodsDialog.dialog({
        mask: true,
        title: "添加新物品",
        content: "url:/static/html/create-goods-dialog.html",
        showBtn: true,
        ok: function() {
            //物品编号
            var number = $createGoodsDialog.find("input[name=number]").val();
            //所在仓库ID
            var warehouseId = $createGoodsDialog.find("input[name=warehouseId]").val();
            //物品名称
            var name = $createGoodsDialog.find("input[name=name]").val();
            //供应商
            var specification = $createGoodsDialog.find("input[name=specification]").val();
            //数量
            var quantity = $createGoodsDialog.find("input[name=quantity]").val();
            //数量单位
            var quantityUnit = $createGoodsDialog.find("select[name=quantityunit] option:selected").text();
            //单价
            var unitprice = $createGoodsDialog.find("input[name=unitprice]").val();
            //价格单位
            var priceUnit = $createGoodsDialog.find("select[name=priceunit] option:selected").text();
            //其他说明
            var other = $createGoodsDialog.find("input[name=other]").val();
            //alert("数量单位+价格单位: " + quantityUnit + "   " + priceUnit);

            //正则匹配，验证输入内容是否正确
            if (/^[A-Za-z0-9_-]+$/.test(number) == false) {
                $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("编号填写错误，不能为空，并且只能填写数字/英文字母/-和_符号！").css("color", "red");
            } else if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(name) == false) {
                $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("名称填写错误，不能为空，并且只能填写汉字/数字/英文字母/-和_符号！").css("color", "red");
            } else if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(company) == false) {
                $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("供应商填写错误，不能为空，并且只能填写汉字/数字/英文字母/-和_符号！").css("color", "red");
            } else if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(supplier) == false) {
                $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("联系人填写错误，不能为空，并且只能填写汉字/数字/英文字母/-和_符号！").css("color", "red");
            } else if (/^[+]?\d+(.\d+)?$/.test(quantity) == false) {
                $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("数量填写错误，不能为空，并且只能填写大于等于0的数字！").css("color", "red");
            } else if (/^[+]?\d+(.\d+)?$/.test(unitprice) == false) {
                $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("单价填写错误，不能为空，并且只能填写大于等于0的数字！").css("color", "red");
            } else { //内容正确，执行
                var testAllOK = function() {
                    //清空错误信息
                    $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("");

                    var newBuyItem = {
                        Goods: {
                            Company: {
                                Name: company //供应商
                            },
                            Warehouse: {
                                Id: obj.Id //仓库ID
                            },
                            Number: number, //物品ID
                            Name: name, //产品名称
                            QuantityUnit: quantityUnit, //数量单位
                            PriceUnit: priceUnit //价格单位
                        },
                        Supplier: supplier,
                        Quantity: parseFloat(quantity), //本次数量
                        Unitprice: parseFloat(unitprice), //单价
                        Totalprice: parseFloat(quantity) * parseFloat(unitprice),
                        Purpose: purpose, //用途
                        Other: other //其他信息
                    };
                    //保存数据
                    var data = postData(JSON.stringify(newBuyItem), "/green/buyitem/create");
                    if (data.Status == 1) {
                        $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("提交数据失败，请再次点击确认按钮！").css("color", "red");
                    } else {
                        $createGoodsDialog.dialog("close");
                        //保存数据后显示列表
                        //goodsList();
                    }
                };
                if (purpose != "") {
                    if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(purpose) == false) {
                        $createGoodsDialog.find("p[id=buyitem-dialog-err]").text("用途填写错误，不能为空，并且只能填写汉字/数字/英文字母/-和_符号！").css("color", "red");
                    } else {
                        testAllOK();
                    }
                } else {
                    testAllOK();
                }

            }
        },
        onload: function() { //dialog加载完成后执行
            if (obj && obj.Id != "") {

            }
            //找到填写"所属仓库"的文本框并赋值，最后设置该文本框不可用
            $createGoodsDialog.find('#warehouse').val(obj.Name).attr('disabled', 'disabled');
            //找到数量单位下拉框
            var $quantityUnit = $createGoodsDialog.find('#quantityunit');
            $quantityUnit.append($("<option value=\"kg\">千克</option>"));
            //找到价格单位下拉框
            var $priceUnit = $createGoodsDialog.find('#priceunit');
            $priceUnit.append($("<option value=\"yuan\">元</option>"));
        }
    });

    //监控编号文本输入框中内容
    $(document).on("input propertychange", "#buyitem-dialog input[name=number]", function() {
        //获取输入内容
        number = $(this).val();
        //正则判断输入内容是否匹配
        if (/^[A-Za-z0-9_-]+$/.test(number) == false) {
            //设置自定义出错提示信息
            this.setCustomValidity("请输入数字/英文字母/-和_符号！");
        } else {
            this.setCustomValidity("");
        }
    });

    //监控名称文本输入框中内容
    $(document).on("input propertychange", "#buyitem-dialog input[name=name]", function() {
        //获取输入内容
        name = $(this).val();
        //正则判断输入内容是否匹配
        if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(name) == false) {
            this.setCustomValidity("请输入汉字/数字/英文字母/-和_符号！");
        } else {
            this.setCustomValidity("");
        }
    });

    //监控供应商文本输入框中内容
    $(document).on("input propertychange", "#buyitem-dialog input[name=company]", function() {
        //获取输入内容
        company = $(this).val();
        //正则判断输入内容是否匹配
        if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(company) == false) {
            this.setCustomValidity("请输入汉字/数字/英文字母/-和_符号！");
        } else {
            this.setCustomValidity("");
        }
    });

    //监控联系人文本输入框中内容
    $(document).on("input propertychange", "#buyitem-dialog input[name=supplier]", function() {
        //获取输入内容
        supplier = $(this).val();
        //正则判断输入内容是否匹配
        if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(supplier) == false) {
            this.setCustomValidity("请输入汉字/数字/英文字母/-和_符号！");
        } else {
            this.setCustomValidity("");
        }
    });

    //监控数量文本输入框中内容
    var unitprice = "",
        quantity = "";
    $(document).on("input propertychange", "#buyitem-dialog input[name=quantity]", function() {
        //获取输入内容
        quantity = $(this).val();
        //正则判断输入内容是否匹配
        if (/^[+]?\d+(.\d+)?$/.test(quantity) == false) {
            $("#item-totalprice").text("");
            this.setCustomValidity('请输入数字！');
        } else {
            this.setCustomValidity("");
            //转换成浮点类型的值
            var q2int = parseFloat(quantity);
            var u2int = parseFloat(unitprice);
            //判断是否是个数字
            if (isNaN(q2int) == false && isNaN(u2int) == false) {
                if (q2int != 0 && u2int != 0) {
                    $("#totalprice").text(q2int * u2int);
                }
            } else {
                $("#totalprice").text("");
            }
        }
    });

    //监控单价文本输入框中内容
    $(document).on("input propertychange", "#buyitem-dialog input[name=unitprice]", function() {
        //获取输入内容
        unitprice = $(this).val();
        //正则判断输入内容是否匹配
        if (/^[+]?\d+(.\d+)?$/.test(unitprice) == false) {
            $("#item-totalprice").text("");
            this.setCustomValidity('请输入数字！');
        } else {
            this.setCustomValidity("");
            //转换成浮点类型的值
            var q2int = parseFloat(quantity);
            var u2int = parseFloat(unitprice);
            //判断是否是个数字
            if (isNaN(q2int) == false && isNaN(u2int) == false) {
                if (q2int != 0 && u2int != 0) {
                    $("#totalprice").text(q2int * u2int);
                }
            } else {
                $("#totalprice").text("");
            }
        }
    });

    //监控用途文本输入框中内容
    $(document).on("input propertychange", "#buyitem-dialog input[name=purpose]", function() {
        //获取输入内容
        purpose = $(this).val();
        if (purpose != "") {
            //正则判断输入内容是否匹配
            if (/^[\u4E00-\u9FA5A-Za-z0-9_-]+$/.test(purpose) == false) {
                this.setCustomValidity("请输入汉字/数字/英文字母/-和_符号！");
            }
        } else {
            this.setCustomValidity("");
        }
    });

    //textarea获取焦点和失去焦点后的反应
    $(document).on("focus", "#buyitem-dialog textarea", function() {
        if ($(this).val() == "其他说明请填写") {
            $(this).val("");
        }
    }).on("blur", "#buyitem-dialog textarea", function() {
        if ($(this).val() == "") {
            $(this).val("其他说明请填写");
        }
    });
}