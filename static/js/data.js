//post数据到后台，成功后返回该记录ID
function postData(jsonData, url) {
    var obj = null;
    $.ajax({
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: url,
        data: jsonData,
        async: false,
        success: function(data) {
            obj = data;
        }
    });
    return obj;
}

//get数据
function getData(url) {
    var obj = null;
    $.ajax({
        type: "get",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: url,
        data: {},
        async: false,
        success: function(data) {
            obj = data;
        }
    });
    return obj;
}

//共享数据
//添加
//share.data("myname", "xxxxxx"); 
//获取
//var name=share.data("myname"); 
//删除
//share.removeData("myname");
var share = {
    /**
     * 跨框架数据共享接口
     * @param {String} 存储的数据名
     * @param {Any} 将要存储的任意数据(无此项则返回被查询的数据)
     */
    data: function(name, value) {
        var top = window.top,
            cache = top['_CACHE'] || {};
        top['_CACHE'] = cache;
        return value ? cache[name] = value : cache[name];
    },
    /**
     * 数据共享删除接口
     * @param {String} 删除的数据名
     */
    removeData: function(name) {
        var cache = window.top['_CACHE'];
        if (cache && cache[name]) delete cache[name];
    }
};