﻿/**
 * $.bwPagination
 * @extends jquery.1.9.0
 * @fileOverview 创建分页导航条
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2013-02-20
 * Copyright (c) 2013-2013 BW
 * @example
 *    $("#digg").bwPagination();
 */
$(function ($) {

	$.fn.bwPagination = function (settings) {
		//默认参数
		var defaultSettings = {
			/**总条数*/
			item_count : 108,
			/**每页显示条数*/
			item_per_page : 10,
			/**显示页码数*/
			display : 9,
			/**url前缀*/
			urlPrefix : "/book/list/"
		}
		/**合并默认参数和用户自定义参数*/
		settings = $.extend({}, defaultSettings, settings);
		//settings = $.extend(defaultSettings, settings);

		return this.each(function () {
			var elem = $(this);
			var page_count_num = getPageCount(settings.item_count, settings.item_per_page);
			var nowPageId = getNowPage();
			var urlPrefix = settings.urlPrefix;
			var page = new Page();

			//根据参数加载
			page.view(elem, page_count_num, settings.display, nowPageId);

			//点击页码跳转
			elem.children("a").mouseover(function () {
				var num = $(this).html();
				var url = urlPrefix + num;
				if (parseInt(num) >= 1 || parseInt(num) <= page_count_num) {
					$(this).attr("href", url);
				}
			});

			//鼠标移上后添加上一页链接
			elem.find("a#pprev").mouseover(function () {
				var nextPage = nowPageId - 1;
				if (nextPage >= 1) {
					var url = urlPrefix + nextPage;
					$(this).attr("href", url);
				}
			});

			//鼠标移上后添加下一页链接
			elem.find("a#pnext").mouseover(function () {
				var nextPage = nowPageId + 1;
				if (nextPage <= page_count_num) {
					var url = urlPrefix + nextPage;
					$(this).attr("href", url);
				}
			});

			//鼠标移上后添加第一页链接
			elem.find("a#pfirst").mouseover(function () {
				var url = urlPrefix + "1";
				$(this).attr("href", url);
			});

			//鼠标移上后添加最后一页链接
			elem.find("a#plast").mouseover(function () {
				var url = urlPrefix + page_count_num;
				$(this).attr("href", url);
			});

			//输入文本后回车转到相关链接
			$(elem).keydown(function (event) {
				if (event.keyCode == 13) {
					var num = elem.find("input#input").val();
					location.href = urlPrefix + num;
				}
			});

		});

	}

	function Page() {}

	Page.prototype = {
		//显示页码
		view : function (elem, page_count, display, nowPageNum) {
			//首页
			var firstPage = "<a id=\"pfirst\">首页</a>";
			//上一页
			var prevPage = "<a id=\"pprev\" class=\"disabled\">上一页</a>";
			//下一页
			var nextPage = "<a id=\"pnext\">下一页</a>";
			//尾页
			var lastPage = "<a id=\"plast\">尾页</a>";
			//当前页
			var nowPage = "<a class=\"current\">" + nowPageNum + "</a>";
			//跳转页
			var jumpPage = "<span>&nbsp;&nbsp;跳转到<input type='text' id=\"input\" class=\"input\" value='' >/" + page_count + "页</span>";

			//清空elem
			elem.empty();

			if (page_count <= display) { //总页数<=显示页码数
				elem.append(firstPage);

				if (nowPageNum == 1) { //当前页为第一页
					elem.append(prevPage);
					elem.append(nowPage);
					for (var i = nowPageNum + 1; i < page_count + 1; i++) {
						elem.append($('<a>' + i + '</a>'));
					}
				} else if (nowPageNum == page_count) { //当前页为最后一页
					elem.append($(prevPage).removeAttr("class"));
					for (var i = 1; i < page_count + 1; i++) {
						elem.append($('<a>' + i + '</a>'));
					}
					elem.append(nowPage);
					elem.append($(nextPage).attr("class", "disabled"));

				} else { //当前页在中间
					elem.append($(prevPage).removeAttr("class"));
					for (var i = 1; i < nowPageNum; i++) {
						elem.append($('<a>' + i + '</a>'));
					}
					elem.append(nowPage);
					for (var i = nowPageNum + 1; i < page_count + 1; i++) {
						elem.append($('<a>' + i + '</a>'));
					}
					elem.append(nextPage);
				}

				elem.append(lastPage);
				elem.append(jumpPage);
			} else { //总页数>显示页码数
				elem.append(firstPage);
				var a = (display - 1) / 2;
				if (nowPageNum - a <= 0) { //当前页在第一页到第(display - 1) / 2页
					if (nowPageNum == 1) { //当前页为第一页
						elem.append(prevPage);
						elem.append(nowPage);
						for (var i = nowPageNum + 1; i < display + 1; i++) {
							elem.append($('<a>' + i + '</a>'));
						}
					} else { //当前页不是第一页
						elem.append($(prevPage).removeAttr("class"));
						for (var i = 1; i < nowPageNum; i++) {
							elem.append($('<a>' + i + '</a>'));
						}
						elem.append(nowPage);
						for (var i = nowPageNum + 1; i < display + 1; i++) {
							elem.append($('<a>' + i + '</a>'));
						}
					}
					elem.append(nextPage);
				} else if (nowPageNum > page_count - a) { //当前页在第page_count-(display - 1) / 2页到最后一页之间
					elem.append($(prevPage).removeAttr("class"));
					if (nowPageNum == page_count) { //当前页为最后一页
						for (var i = page_count - display + 1; i < page_count; i++) {
							elem.append($('<a>' + i + '</a>'));
						}
						elem.append(nowPage);
						elem.append($(nextPage).attr("class", "disabled"));
					} else { //当前页不是最后一页
						for (var i = page_count - display + 1; i < nowPageNum; i++) {
							elem.append($('<a>' + i + '</a>'));
						}
						elem.append(nowPage);
						for (var i = nowPageNum + 1; i < page_count + 1; i++) {
							elem.append($('<a>' + i + '</a>'));
						}
						elem.append(nextPage);
					}

				} else { //当前页在中间
					elem.append($(prevPage).removeAttr("class"));
					for (var i = nowPageNum - a; i < nowPageNum; i++) {
						elem.append($('<a>' + i + '</a>'));
					}
					elem.append(nowPage);
					for (var i = nowPageNum + 1; i < nowPageNum + a + 1; i++) {
						elem.append($('<a>' + i + '</a>'));
					}
					elem.append(nextPage);
				}
				elem.append(lastPage);
				elem.append(jumpPage);
			}
			return elem;
		}

	}

	//获取总页数
	function getPageCount(item_count, item_per_page) {
		if (item_count >= item_per_page) {
			if (item_count % item_per_page > 0) {
				page_count = parseInt(item_count / item_per_page) + 1;
			} else if (item_count % item_per_page == 0) {
				page_count = parseInt(item_count / item_per_page);
			}
		} else {
			page_count = 1;
		}
		return page_count;
	}

	//获取当前页码
	function getNowPage() {
		var nowUrl = location.pathname;
		var nowPageNum = parseInt(nowUrl.replace(/[^\d]/g, ''));
		return nowPageNum;
	}

})(jQuery);
