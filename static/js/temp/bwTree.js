﻿/**
 * $.bwTree
 * @extends jquery.1.9.0
 * @fileOverview 创建Tree
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2013-02-20
 * Copyright (c) 2013-2013 BW
 * @example
 *    $("#tree").bwTree();
 */
(function($) {

	$.fn.bwTree = function(settings) {
		//默认参数
		var defaultSettings = {
			/**缺省ID字段名*/
			Id: "Id",
			/**缺省父ID字段名*/
			Fid: "Fid",
			/**缺省级别字段名*/
			Level: "Level",
			/**缺省名称字段名*/
			Name: "Name",
			/**缺省URL*/
			TUrl: "green/category/list"
		}
		/**合并默认参数和用户自定义参数*/
		settings = $.extend({}, defaultSettings, settings);

		var drawTree = function(elem, Id, Fid, Level, Name, TUrl) {
			$.ajax({
				type: "post",
				dataType: "json",
				url: TUrl,
				data: "{}",
				//async : false,
				success: function(data) {
					$.each(data, function(k, v) {
						if (Id != "Id") {
							data[k]["Id"] = data[k][Id];
							data[k][Id] = undefined;
						}
						if (Fid != "Fid") {
							data[k]["Fid"] = data[k][Fid];
							data[k][Fid] = undefined;
						}
						if (Name != "Name") {
							data[k]["Name"] = data[k][Name];
							data[k][Name] = undefined;
						}
					});

					$.each(data, function(k, v) {
						if (v.Fid == 0) {
							elem.append($("<li id=c" + v.Id + "><a href=\"#\">" + v.Name + "</a></li>"));
							$("#c" + v.Id).bind("click", function() {
								changeDiv(v);
								return false;
							});

						} else {
							$("#c" + v.Fid).append($("<ul><li id=c" + v.Id + "><a href=\"#\">" + v.Name + "</a></li></ul>"));
							$("#c" + v.Id).bind("click", function() {
								changeDiv(v);
								return false;
							});
						}

					});
				}
			});
			return elem;
		}

		//点击分类名后改变DIV中的内容
		var changeDiv = function(v) {
			$("#content").empty().append($("<a id=\"add" + v.Id + "\">添加</a>")).append($("<div id=\"listDiv\"></div>"))
			$("#add" + v.Id).bind("click", function() {
				$("#main").popupDiv({
					title: "请添加一条记录"
				}, v);
			});
			$.ajax({
				type: "post",
				dataType: "json",
				url: "green/item/list",
				data: "{}",
				//async : false,
				success: function(data) {
					$.each(data, function(k, v) {
						$("#listDiv").append($("<li><span>" + v.Zhuangtai + "</span></li>"));
					});
				}
			});
			var listDiv = "<div><p>ID:" + v.Id + "</p><p>父ID:" + v.Fid + "</p><p>排序级别:" + v.Level + "</p><p>分类名称:" + v.Name + "</p></div>";


		}

		return this.each(function() {
			var elem = $("#tree");
			drawTree(elem, settings.Id, settings.Fid, settings.Level, settings.Name, settings.TUrl);
		});

	}

})(jQuery);

/**
 * $.popupDiv
 * @extends jquery.1.9.0
 * @fileOverview 弹出层
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2013-02-20
 * Copyright (c) 2013-2013 BW
 * @example
 *    $("#myDIV").popupDiv();
 */
(function($) {

	$.fn.popupDiv = function(settings, e) {
		var defaultSettings = {
			//提交数据到后台地址
			postUrl: "green/item/create",
			contentUrl: "/static/html/popupDivContent.html",
			title: "添加一条记录",
			width: 500,
			height: 400
		};
		var settings = $.extend({}, defaultSettings, settings);

		return this.each(function() {
			popupDiv();
		});

		function popupDiv() {

			var maskDiv = "<div id=\"mask\" class=\"mask\"></div>";

			var popupDiv = "<div id=\"popupDiv\"></div>";
			var popupTitle = "<div id=\"pop-box-title\"><h4>" + settings.title + "</h4></div>";
			var popupContent = "<div id=\"pop-box-body\"></div>";
			var popupButtonPanel = "<div class=\"buttonPanel\" style=\"text-align: right\" style=\"text-align: right\"><a id=\"btn2\">添加</a>||<a id=\"btn1\">取消</a></div>";

			var windowWidth = $(window).width();
			var windowHeight = $(window).height();
			var popupHeight = settings.height;
			var popupWidth = settings.width;

			mask_obj = $(maskDiv);
			$("body").append(mask_obj).append($(popupDiv));
			var div_obj = $("#popupDiv").append($(popupTitle)).append($(popupContent)).append($(popupButtonPanel));

			//加载内容模板
			$("#pop-box-body").empty().load(settings.contentUrl);

			//添加并显示遮罩层
			mask_obj.css({
				"display": "block",
				"position": "absolute",
				"background-color": "gray",
				"left": "0px",
				"top": "0px",
				"width": windowWidth + "px",
				"height": windowHeight + "px",
				"z-index": "10000"
			})
				.fadeIn(200);

			//弹出层
			div_obj.css({
				"position": "absolute",
				"background-color": "white",
				"left": windowWidth / 2 - popupWidth / 2 + "px",
				"top": windowHeight / 2 - popupHeight / 2 + "px",
				"width": popupWidth + "px",
				"height": popupHeight + "px",
				"border": "solid 1px red",
				"z-index": "10001"
			})

			//点击取消按钮
			$("#btn1").click(function() {
				deleteDiv();
			});
			//点击提交按钮
			$("#btn2").click(function() {
				var i
				if ($("#pop_zhuangtai").val() == "入库") {
					i = 0
				} else if ($("#pop_zhuangtai").val() == "出库") {
					i = 1
				}
				var bitem = {
					Caozuoren: $("#pop_caozhuren").val(),
					Shuliang: $("#pop_shuliang").val(),
					Zhuangtai: i,
					Beizhu: $("#pop_beizhu").val(),
					Name: e.Name
				};
				//alert("操作人：" + $("#pop_caozhuren").val() + "数量：" + $("#pop_shuliang").val() + "操作：" + $("#pop_zhuangtai").val() + "备注：" + $("#pop_beizhu").val() + "分类ID：" + e.Id);
				postData(JSON.stringify(bitem));
				deleteDiv();
			});

			//按下鼠标拖动弹出层
			$("#pop-box-title").mousedown(function(e) {
				//改变鼠标指针的形状
				$(this).css("cursor", "move");
				//DIV在页面的位置
				var offset = $(this).offset();
				//获得鼠标指针离DIV元素左边界的距离
				var x = e.pageX - offset.left;
				//获得鼠标指针离DIV元素上边界的距离
				var y = e.pageY - offset.top;
				//绑定鼠标的移动事件，因为光标在DIV元素外面也要有效果，所以要用doucment的事件，而不用DIV元素的事件
				$(document).bind("mousemove", function(ev) {
					//停止当前动画效果
					div_obj.stop();
					//获得X轴方向移动的值
					var _x = ev.pageX - x;
					//获得Y轴方向移动的值
					var _y = ev.pageY - y;

					//移动层
					div_obj.animate({
						left: _x + "px",
						top: _y + "px"
					}, 10);
				});

			});
			//松开鼠标
			$(document).mouseup(function() {
				//改变鼠标指针的形状
				div_obj.css("cursor", "default");
				//移出mousemove事件
				$(this).unbind("mousemove");
			});
		}

		//提交数据到后台

		function postData(bitem) {
			$.ajax({
				type: "post",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				url: settings.postUrl,
				data: bitem,
				//async : false,
				success: function() {
					alert("提交成功");
				}
			});
		}

		//删除层

		function deleteDiv() {
			$("#mask").remove();
			$("#popupDiv").remove();
		}

	}

})(jQuery);