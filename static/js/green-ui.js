/**
 * $.dialog
 * @extends jquery-1.10.2
 * @fileOverview 弹出层
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2014-2-16
 * Copyright (c) 2013-2014 BW
 * @example
 * $("#box").dialog({
 *    move: true,
 *    title: "添加分类",
 *    buttons: [{
 *       name: "重置",
 *       callback: function() {
 *          alert("重置");
 *       }
 *    }, {
 *       name: "上一步",
 *       callback: function() {
 *          alert("上一步");
 *    }
 *    }]
 * });
 */
(function($) {
	var dialogId = "";
	$.fn.dialog = function() {
		var method = arguments[0];
		if (methods[method]) {
			method = methods[method];
			// 我们的方法是作为参数传入的，把它从参数列表中删除，因为调用方法时并不需要它
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if (typeof(method) == "object" || !method) {
			method = methods.init;
		} else {
			$.error("Method" + method + "does not exist on jQuery.dialog");
			return this;
		}
		// 用apply方法来调用我们的方法并传入参数
		return method.apply(this, arguments);
	};

	var methods = {
		init: function(options) {
			var defaults = {
				move: false,
				mask: false,
				width: "auto",
				height: "auto",
				zindex: "1",
				title: false,
				//content: "url:http://localhost:8080/static/temp/dialog/content.html",
				//content: "url:content.html",
				content: "请稍候...",
				//content:$div,
				showBtn: true,
				okText: "确认",
				cancelText: "取消",
				ok: function() {},
				cancel: function() {
					methods.close();
				},
				buttons: [],
				onload: function() {} //通过load()加载内容完成后执行的函数
			};

			var settings = $.extend({}, defaults, options);

			return this.each(function() {
				var $dialog = $(this);
				$dialog.addClass("green-ui-dialog"); //把dom加入到dialog中，并添加class
				dialogId = $dialog.attr("id");

				//打开遮蔽层
				if (settings.mask == true) {
					$("body").append($("<div id=\"" + dialogId + "-mask\" class=\"green-ui-dialog-mask\"></div>"));
					$("#" + dialogId + "-mask").addClass("green-ui-dialog-mask");
				}

				//画标题BOX
				if (typeof(settings.title) == "string") { //显示标题
					var $titleBox = $("<div class=\"green-ui-dialog-titleBox\"><h3></h3><p><span>X</span></p></div>").attr("id", dialogId + "-titleBox");
					$titleBox.find("h3").text(settings.title); //找到h3标签，并设置此标签文本内容
					$titleBox.find("span").attr("id", dialogId + "-close"); //找到span标签，并设置此标签id属性
					$dialog.append($titleBox);
					$titleBox.css("line-height", $titleBox.height() + "px"); //设置div标签css行高
				}

				//画内容BOX
				var $contentBox = $("<div class=\"green-ui-dialog-contentBox\"></div>").attr("id", dialogId + "-contentBox");
				$dialog.append($contentBox);


				//画按钮BOX
				if (settings.showBtn == true) { //显示按钮组
					var $buttonBox = $("<div class=\"green-ui-dialog-buttonBox\"></div>").attr("id", dialogId + "-buttonBox");
					//把cancel和ok按钮加进buttons按钮组
					var cancel = {
						name: settings.cancelText,
						callback: settings.cancel
					};
					var ok = {
						name: settings.okText,
						callback: settings.ok
					};
					settings.buttons.unshift(cancel, ok); //把按钮加到数组最前面
					//循环添加每一个按钮到buttonBox中
					$.each(settings.buttons, function(k, v) {
						$buttonBox.append($("<p><span id=\"" + dialogId + "-btn-" + k + "\" class=\"green-ui-dialog-button\">" + v.name + "</span></p>"));
					});
					$dialog.append($buttonBox);
				}

				//设置Dialog自适应宽度和高度，并居中
				var setDialog = function() {
					var width = settings.width; //取得自定义的宽度
					var height = settings.height; //取得自定义的高度
					var buttonBoxStr = "#" + dialogId + "-buttonBox";
					if (typeof(width) == "number") { //如果是个数字
						$dialog.css("width", width); //设置为dialog的宽度
					}
					if (typeof(height) == "number") { //如果是个数字
						$dialog.css("height", height); //设置为dialog的高度
						var titleBoxHeight = 0;
						var btnBoxHeight = 0;
						if (typeof(settings.title) == "string") { //如果有标题栏
							titleBoxHeight = $(titleBoxStr).height(); //取得标题栏高度
						}
						if (settings.showBtn == true) { //如果有按钮栏
							btnBoxHeight = $(buttonBoxStr).height(); //取得按钮栏高度
						}
						var contentBoxHeight = height - titleBoxHeight - btnBoxHeight - 5; //得到内容栏高度

						$("#" + dialogId + "-contentBox").css("height", contentBoxHeight + "px"); //设置内容栏高度
						//设置按钮栏为最底部
						$(buttonBoxStr).css({
							"position": "absolute",
							"bottom": "0px"
						});
					}

					var width = $dialog.width();
					var height = $dialog.height();

					//设置dialog居中
					$dialog.css({
						"display": "inline",
						"left": "50%",
						"top": "50%",
						"margin": "-" + (height / 2) + "px 0 0 -" + (width / 2) + "px",
						"width": width + "px",
						"height": height + "px",
						"z-index": settings.zindex
					});
				};

				//画内容详细
				if (typeof(settings.content) == "object") { //有object前缀，表示直接加载对象
					var $object = settings.content;
					$contentBox.append($object);
					$contentBox.width($object.width()).height($object.height());
					setDialog();
				} else {
					if (settings.content.indexOf("url:") != -1) { //有url前缀，表示直接加载html
						$contentBox.load(settings.content.split("url:")[1], function() {
							setDialog();
							settings.onload();
						});
					} else { //无object前缀,无url前缀，表示直接加载文本
						var $span = $("<span></span>").text(settings.content); //设置span标签的文本内容
						$contentBox.append($span);
						setDialog();
					}
				}

				//添加事件
				var bindAll = function() {
					//为title关闭按钮添加鼠标移入移出事件和click事件
					var divCloseStr = "#" + dialogId + "-close";
					var titleBoxStr = "#" + dialogId + "-titleBox";
					if (typeof(settings.title) == "string") {
						$(document).on("mouseover", divCloseStr, function() {
							$(divCloseStr).addClass("green-ui-dialog-close");
						}).on("mouseout", divCloseStr, function() {
							$(divCloseStr).removeClass("green-ui-dialog-close");
						});
						$(document).on("click", divCloseStr, function() {
							$dialog.dialog("close");
						});
					}

					//如果dialog可以移动，就添加移动事件
					if (settings.move == true) {
						$(titleBoxStr).mouseover(function() {
							$dialog.css("cursor", "move");
						}).mouseout(function() {
							$dialog.css("cursor", "default");
						}).mousedown(function(event) {
							$dialog.append($("<div id=\"" + dialogId + "-hide\"></div>"));
							$("#" + dialogId + "-hide").css({
								"position": "relative",
								"width": $dialog.width(),
								"height": $dialog.height() - $(titleBoxStr).height(),
								"top": "-" + ($dialog.height() - $(titleBoxStr).height()) + "px",
								"z-index": "10002"
							});
							var offset = $dialog.offset();
							var x1 = event.pageX - offset.left;
							var y1 = event.pageY - offset.top;
							var btnNum = event.which;
							if (btnNum == 1) {
								$(document).mousemove(function(event) {
									$("#" + dialogId + "-iframeBox").contents().find("p").text("x:" + event.pageX + "  y:" + event.pageY);
									$dialog.css({
										"left": (event.pageX - x1) + "px",
										"top": (event.pageY - y1) + "px",
										"margin": "0"
									});
								});
							}
						}).mouseup(function() {
							$("#" + dialogId + "-hide").remove();
							$(document).unbind("mousemove");
						});;

					}

					//为每个按钮添加鼠标移入移出事件和点击事件
					if (settings.showBtn == true) {
						$.each(settings.buttons, function(k, v) {
							var str = dialogId + "-btn-" + k;
							$("#" + str).mouseover(function() {
								$("#" + str).addClass("green-ui-dialog-button-change");
							}).mouseout(function() {
								$("#" + str).removeClass("green-ui-dialog-button-change");
							});

							$(document).on("click", "#" + str, function() {
								v.callback();
							});
						});
					}
				};

				bindAll();
				//-----------------------------------------------------------------
			});
		},
		close: function() {
			$("#" + dialogId + "-mask").remove();
			$("#" + dialogId).remove();
		}
	};

})(jQuery);


/**
 * $.table
 * @extends jquery-1.10.2
 * @fileOverview 表格
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2014-5-29
 * Copyright (c) 2013-2014 BW
 * @example
 
 */
(function($) {
	var tableId = "";
	$.fn.table = function() {
		var method = arguments[0];
		if (methods[method]) {
			method = methods[method];
			// 我们的方法是作为参数传入的，把它从参数列表中删除，因为调用方法时并不需要它
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if (typeof(method) == "object" || !method) {
			method = methods.init;
		} else {
			$.error("Method" + method + "does not exist on jQuery.dialog");
			return this;
		}
		// 用apply方法来调用我们的方法并传入参数
		return method.apply(this, arguments);
	};

	var methods = {
		init: function(options) {
			var defaults = {
				url: "",
				filter: [],
				otherName: "",
				title: "标题"
			};

			var settings = $.extend({}, defaults, options);

			return this.each(function() {
				//获取数据
				var getData = function(url) {
					var obj = null;
					$.ajax({
						type: "post",
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						url: url,
						data: {},
						async: false,
						success: function(resData) {
							obj = resData;
						}
					});
					return obj;
				};
				var goodsList = getData(settings.url);

				var $table = $(this);
				tableId = $table.attr("id");
				//$("<table class=\"green-ui-table\"></table>").appendTo($table);
				$table.addClass('green-ui-table').appendTo($table);

				var tableTrFirst = tableId + "-table-tr-first";
				if (typeof(settings.otherName) == "object") {

					$table.append($("<tr id=\"" + tableTrFirst + "\"></tr>").addClass('green-ui-table-tr-first'));
					$.each(settings.otherName, function(index, val) {
						$("<th>" + val + "</th>").appendTo($("#" + tableTrFirst));
					});

					$.each(goodsList, function(k, v) {
						var str = tableId + "-table-tr-" + k;
						$table.append($("<tr id=\"" + str + "\"></tr>"));
						$.each(settings.otherName, function(key, value) {
							$.each(v, function(index, val) {
								if (index == key) {
									$("<td>" + val + "</td>").appendTo($("#" + str));
								}
							});
						});
					});
				} else {
					var drawTable = function(str, v, ok) {
						$table.append($("<tr id=\"" + str + "\"></tr>").addClass('green-ui-table-tr-first'));
						$.each(v, function(index, val) {
							var i = 0;
							if (settings.filter.length > 0) {
								$.each(settings.filter, function(key, value) {
									if (value == index) {
										i = 1;
									}
								});
							}
							if (i == 0) {
								if (ok == true) {
									$("<th>" + index + "</th>").appendTo($("#" + str));
								} else {
									$("<td>" + val + "</td>").appendTo($("#" + str));
								}
							}
						});
					};
					$.each(goodsList, function(k, v) {
						if (k == 0) {
							drawTable(tableTrFirst, v, true);

						}
						var str = tableId + "-table-tr-" + k;
						drawTable(str, v, false);
					});
				}

				//奇偶行不同颜色
				$table.find("tr:gt(0):odd").addClass("green-ui-table-tr-odd");
				$table.find("tr:gt(0):even").addClass("green-ui-table-tr-even");

				$table.find('tr:gt(0)').hover(
					function() {
						$(this).addClass('green-ui-table-tr-hover');
					},
					function() {
						$(this).removeClass('green-ui-table-tr-hover');
					}
				);

				// if (typeof(settings.otherName) == "object") {
				// 	var str = tableId + "-table-p-menu";
				// 	$table.find('table').append($("<tr id=\"" + str + "\"></tr>"));
				// 	$.each(settings.otherName, function(index, val) {
				// 		$("<th>" + val + "</th>").appendTo($("#" + str));
				// 	});

				// 	$.each(goodsList, function(k, v) {
				// 		var str = tableId + "-table-p-" + k;
				// 		$table.find('table').append($("<tr id=\"" + str + "\"></tr>"));
				// 		$.each(settings.otherName, function(key, value) {
				// 			$.each(v, function(index, val) {
				// 				if (index == key) {
				// 					$("<td>" + val + "</td>").appendTo($("#" + str));
				// 				}
				// 			});
				// 		});
				// 	});
				// } else {
				// 	var drawTable = function(str, v, ok) {
				// 		$table.find('table').append($("<tr id=\"" + str + "\"></tr>"));
				// 		$.each(v, function(index, val) {
				// 			var i = 0;
				// 			if (settings.filter.length > 0) {
				// 				$.each(settings.filter, function(key, value) {
				// 					if (value == index) {
				// 						i = 1;
				// 					}
				// 				});
				// 			}
				// 			if (i == 0) {
				// 				if (ok == true) {
				// 					$("<th>" + index + "</th>").appendTo($("#" + str));
				// 				} else {
				// 					$("<td>" + val + "</td>").appendTo($("#" + str));
				// 					// $("#" + str).hover(
				// 					// 	function() {
				// 					// 		$("#" + str).addClass('green-ui-table-tr-hover');
				// 					// 	},
				// 					// 	function() {
				// 					// 		$("#" + str).removeClass('green-ui-table-tr-hover');
				// 					// 	}
				// 					// );
				// 				}
				// 			}
				// 		});
				// 	};
				// 	$.each(goodsList, function(k, v) {
				// 		if (k == 0) {
				// 			var str = tableId + "-table-p-menu";
				// 			drawTable(str, v, true);

				// 		}
				// 		var str = tableId + "-table-p-" + k;
				// 		drawTable(str, v, false);
				// 	});
				// }
				// var nowTable = $table.find('table');
				// //奇偶行不同颜色
				// $("#table2 tbody tr:odd").addClass("odd"),
				// $("#table2 tbody tr:even").addClass("even")

				// $table.find('table').find('tr:gt(0)').hover(
				// 	function() {
				// 		$(this).addClass('green-ui-table-tr-hover');
				// 	},
				// 	function() {
				// 		$(this).removeClass('green-ui-table-tr-hover');
				// 	}
				// );
			});
		},
		close: function() {
			//$("#" + dialogId + "-mask").remove();
			//$("#" + dialogId).remove();
		}
	};

})(jQuery);