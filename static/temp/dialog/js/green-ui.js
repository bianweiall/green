/**
 * $.dialog
 * @extends jquery-1.10.2
 * @fileOverview 弹出层
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2014-2-16
 * Copyright (c) 2013-2014 BW
 * @example
 * $("#box").dialog({
move: true,
title: "添加分类",
buttons: [{
name: "重置",
callback: function() {
alert("重置");
}
}, {
name: "上一步",
callback: function() {
alert("上一步");
}
}]
}).dialog("open");
 */
(function($) {
	var idName;
	$.fn.dialog = function() {
		var method = arguments[0];
		if (methods[method]) {
			method = methods[method];
			// 我们的方法是作为参数传入的，把它从参数列表中删除，因为调用方法时并不需要它
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if (typeof(method) == "object" || !method) {
			method = methods.init;
		} else {
			$.error("Method" + method + "does not exist on jQuery.dialog");
			return this;
		}
		// 用apply方法来调用我们的方法并传入参数
		return method.apply(this, arguments);
	};

	var methods = {
		init: function(options) {
			var defaults = {
				move: false,
				lock: false,
				width: "auto",
				height: "auto",
				title: false,
				content: "url:http://localhost:8080/static/temp/dialog/content.html",
				//content: "url:content.html",
				//content: "正在准备内容...正在准备内容...正在准备内容...正在准备内容...",
				showBtn: true,
				okText: "确认",
				cancelText: "取消",
				ok: function() {},
				cancel: function() {
					if (settings.lock == true) {
						$("#" + idName + "-lock").remove();
					}
					methods.close();
				},
				buttons: []
			};

			var settings = $.extend({}, defaults, options);

			return this.each(function() {
				var $this = $(this);
				idName = $this.attr("id");
				var isIframe = settings.content.indexOf("url:");

				var htmlStr = "<div class=\"green-ui-dialog-titleBox\"><h3></h3><p><span>X</span></p></div>" +
					"<div class=\"green-ui-dialog-contentBox\"><span></span><iframe scrolling=\"no\" frameborder=\"no\"></iframe></div>" +
					"<div class=\"green-ui-dialog-buttonBox\"></div>";

				//画出Dialog
				var drawDialog = function() {

					var dom = $(htmlStr);
					$this.append(dom).addClass("green-ui-dialog"); //把dom加入到dialog中，并添加class

					//标题BOX
					if (typeof(settings.title) == "string") { //显示标题
						var dom_div = dom.eq(0).attr("id", idName + "-titleBox"); //找到第1个div标签，并设置id属性
						dom_div.css("line-height", dom_div.height() + "px"); //设置div标签css行高
						dom_div.find("h3").text(settings.title); //找到h3标签，并设置此标签文本内容
						dom_div.find("span").attr("id", idName + "-close"); //找到span标签，并设置此标签id属性
					}

					//内容BOX
					var dom_div = dom.eq(1).attr("id", idName + "-contentBox"); //找到第2个div标签，并设置id属性
					if (isIframe == -1) { //不是一个url地址
						dom_div.find("iframe").remove(); //找到并删除iframe标签
						dom_div.find("span").text(settings.content); //找到sapn标签，并设置此标签的文本内容
						setDialog();
					} else { //是一个url地址
						dom_div.find("span").remove(); //找到并删除span标签
						//找到iframe标签，并设置此标签的id和src属性
						var dom_iframe = dom_div.find('iframe').attr({
							"id": idName + "-iframeBox",
							"src": settings.content.split("url:")[1]
						});
						//此iframe标签加载完成后获取内容的宽高，并设置此标签的宽度和高度
						dom_iframe.load(function() {
							var iframeBoxHeight = dom_iframe.contents().find("body div:first").height() + 2;
							var iframeBoxWidth = dom_iframe.contents().find("body div:first").width() + 2;
							dom_iframe.height(iframeBoxHeight).width(iframeBoxWidth);
							setDialog();
						});
					}

					//按钮BOX
					if (settings.showBtn == true) { //显示按钮组
						var dom_div = dom.eq(2).attr("id", idName + "-buttonBox"); //找到第3个div标签，并设置id属性

						//把cancel和ok按钮加进buttons按钮组
						var cancel = {
							name: settings.cancelText,
							callback: settings.cancel
						};

						var ok = {
							name: settings.okText,
							callback: settings.ok
						};

						settings.buttons.unshift(cancel, ok); //把按钮加到数组最前面
						//settings.buttons.push(cancel, ok);

						//循环添加每一个按钮到buttonBox中
						$.each(settings.buttons, function(k, v) {
							dom_div.append($("<p><span id=\"" + idName + "-btn-" + k + "\" class=\"green-ui-dialog-button\">" + v.name + "</span></p>"));
						});

					}

					//打开遮蔽层
					if (settings.lock == true) {
						$("body").append($("<div id=\"" + idName + "-lock\"></div>"));
						$("#" + idName + "-lock").css({
							"display": "block",
							"position": "absolute",
							"background-color": "gray",
							"top": "0px",
							"left": "0px",
							"width": "100%",
							"height": "100%",
							"z-index": "10000"
						});
					}

				};

				var titleBoxStr = "#" + idName + "-titleBox";
				var buttonBoxStr = "#" + idName + "-buttonBox";
				var divCloseStr = "#" + idName + "-close";

				//设置Dialog自适应宽度和高度，并居中
				var setDialog = function() {
					var width = settings.width; //取得自定义的宽度
					var height = settings.height; //取得自定义的高度
					if (typeof(width) == "number") { //如果是个数字
						$this.css("width", width); //设置为dialog的宽度
					}
					if (typeof(height) == "number") { //如果是个数字
						$this.css("height", height); //设置为dialog的高度
						var titleBoxHeight = 0;
						var btnBoxHeight = 0;
						if (typeof(settings.title) == "string") { //如果有标题栏
							titleBoxHeight = $(titleBoxStr).height(); //取得标题栏高度
						}
						if (settings.showBtn == true) { //如果有按钮栏
							btnBoxHeight = $(buttonBoxStr).height(); //取得按钮栏高度
						}
						var contentBoxHeight = height - titleBoxHeight - btnBoxHeight - 5; //得到内容栏高度

						$("#" + idName + "-contentBox").css("height", contentBoxHeight + "px"); //设置内容栏高度
						//设置按钮栏为最底部
						$(buttonBoxStr).css({
							"position": "absolute",
							"bottom": "0px"
						});
					}

					//设置dialog居中
					$this.css({
						"left": "50%",
						"top": "50%",
						"margin": "-" + ($this.height() / 2) + "px 0 0 -" + ($this.width() / 2) + "px",
						"width": $this.width() + "px",
						"height": $this.height() + "px",
						"z-index": "10001"
					});
				};

				//添加事件
				var bindAll = function() {
					//为title关闭按钮添加鼠标移入移出事件和click事件
					$(divCloseStr).mouseover(function() {
						$(divCloseStr).addClass("green-ui-dialog-close");
					}).mouseout(function() {
						$(divCloseStr).removeClass("green-ui-dialog-close");
					});
					$this.on("click", divCloseStr, function() {
						if (settings.lock == true) {
							$("#" + idName + "-lock").remove();
						}
						methods.close();
					});

					//如果dialog可以移动，就添加移动事件
					if (settings.move == true) {
						$(titleBoxStr).mouseover(function() {
							$this.css("cursor", "move");
						}).mouseout(function() {
							$this.css("cursor", "default");
						}).mousedown(function(event) {
							$this.append($("<div id=\"" + idName + "-hide\"></div>"));
							$("#" + idName + "-hide").css({
								"position": "relative",
								"width": $this.width(),
								"height": $this.height() - $(titleBoxStr).height(),
								"top": "-" + ($this.height() - $(titleBoxStr).height()) + "px",
								"z-index": "10002"
							});
							var offset = $this.offset();
							var x1 = event.clientX - offset.left;
							var y1 = event.clientY - offset.top;
							var btnNum = event.which;
							if (btnNum == 1) {
								$(document).mousemove(function(event) {
									$("#" + idName + "-iframeBox").contents().find("p").text("x:" + event.clientX + "  y:" + event.clientY);
									$this.css({
										"left": (event.clientX - x1) + "px",
										"top": (event.clientY - y1) + "px",
										"margin": "0"
									});
								});
							}
						}).mouseup(function() {
							$("#" + idName + "-hide").remove();
							$(document).unbind("mousemove");
						});;

					}

					//为每个按钮添加鼠标移入移出事件和点击事件
					$.each(settings.buttons, function(k, v) {
						var str = idName + "-btn-" + k;
						$("#" + str).mouseover(function() {
							$("#" + str).addClass("green-ui-dialog-button-change");
						}).mouseout(function() {
							$("#" + str).removeClass("green-ui-dialog-button-change");
						});

						$(document).on("click", "#" + str, function() {
							v.callback();
						});
					});

				};

				drawDialog();
				bindAll();

			});

		},
		open: function() {
			return this.each(function() {
				var $this = $(this);
				$this.css("display", "inline");
			});
		},
		close: function() {
			//$("#" + idName).empty().css("display", "none");
			$("#" + idName).remove();
		}
	};

})(jQuery);