/**
 * $.dialog
 * @extends jquery.1.9.0
 * @fileOverview 弹出层
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2013-12-18
 * Copyright (c) 2013-2013 BW
 * @example
 * $("#box").dialog({
       move: true,
       title: "添加分类",
       buttons: [{
         name: "重置",
         callback: function() {
           alert("重置");
         }
       }, {
         name: "上一步",
         callback: function() {
           alert("上一步");
         }
       }]
   }).dialog("open");
 */
(function($) {
	var idName;
	$.fn.dialog = function() {
		var method = arguments[0];
		if (methods[method]) {
			method = methods[method];
			// 我们的方法是作为参数传入的，把它从参数列表中删除，因为调用方法时并不需要它
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if (typeof(method) == "object" || !method) {
			method = methods.init;
		} else {
			$.error("Method" + method + "does not exist on jQuery.dialog");
			return this;
		}
		// 用apply方法来调用我们的方法并传入参数
		return method.apply(this, arguments);
	};

	var methods = {
		init: function(options) {
			var defaults = {
				move: false,
				lock: false,
				width: "auto",
				height: "auto",
				title: false,
				content: "url:content.html",
				//content: "正在准备内容...正在准备内容...正在准备内容...正在准备内容...",
				showBtn: true,
				okText: "确认",
				cancelText: "取消",
				ok: function() {},
				cancel: function() {
					methods.close();
				},
				buttons: []
			};

			var settings = $.extend({}, defaults, options);

			return this.each(function() {
				var $this = $(this);
				idName = $this.attr("id");
				var isIframe = settings.content.indexOf("url:");

				if (settings.lock == true) {
					$("body").append($("<div id=\"" + idName + "-lock\"></div>"));
					$("#" + idName + "-lock").css({
						"display": "block",
						"position": "absolute",
						"background-color": "gray",
						"top": "0px",
						"left": "0px",
						"width": "100%",
						"height": "100%",
						"z-index": "10000"
					});
				}

				var htmlStr = "<div class=\"green-ui-dialog-titleBox\"><h3></h3><p><span>X</span></p></div>" +
					"<div class=\"green-ui-dialog-contentBox\"><span></span><iframe></iframe></div>" +
					"<div class=\"green-ui-dialog-buttonBox\"></div>";

				//画出Dialog
				var drawDialog = function() {
					$this.addClass("green-ui-dialog");

					var ddom = $(htmlStr);
					//标题BOX
					if (typeof(settings.title) == "string") {
						//var titleBox = "<div id=\"" + idName + "-titleBox\" class=\"green-ui-dialog-titleBox\"><h3>" + settings.title + "</h3><p><span id=\"" + idName + "-close\">X</span></p></div>";
						$this.append($(titleBox));
					}

					//内容BOX
					//var contentBox = "<div id=\"" + idName + "-contentBox\" class=\"green-ui-dialog-contentBox\"></div>";
					$this.append($(contentBox));

					//内容BOX里面的span || iframe
					if (isIframe == -1) {
						$("<span>" + settings.content + "</span>").appendTo($("#" + idName + "-contentBox"));
					} else {
						var urlStr = settings.content.split("url:")[1];
						$("<iframe id=\"" + idName + "-iframeBox\" src=\"" + urlStr + "\" scrolling=\"no\" frameborder=\"no\"></iframe>").appendTo($("#" + idName + "-contentBox"));
						$("#" + idName + "-iframeBox").load(function() {
							var iframeBoxHeight = $("#" + idName + "-iframeBox").contents().find("body div:first").height() + 2;
							var iframeBoxWidth = $("#" + idName + "-iframeBox").contents().find("body div:first").width() + 2;
							$("#" + idName + "-iframeBox").height(iframeBoxHeight).width(iframeBoxWidth);
							//alert(navigator.userAgent);
							//alert("width:" + $("#" + idName + "-iframeBox").width() + " height:" + $("#" + idName + "-iframeBox").height());

						});

					}

					//按钮BOX(如果settings.showBtn == true，显示按钮组)
					if (settings.showBtn == true) {
						//定义一个buttonBox
						var buttonBox = $("<div id=\"" + idName + "-buttonBox\" class=\"green-ui-dialog-buttonBox\"></div>");
						//定义一个数组，作用是保持每个按钮的名称
						var allBtns = new Array(idName + "-btn-ok", idName + "-btn-cancel");
						//定义两个按钮cancel/ok，并放入buttonBox
						$("<p><span id=\"" + idName + "-btn-cancel\" class=\"green-ui-dialog-button\">" + settings.cancelText + "</span></p>").appendTo(buttonBox);
						$("<p><span id=\"" + idName + "-btn-ok\" class=\"green-ui-dialog-button\">" + settings.okText + "</span></p>").appendTo(buttonBox);

						//定义按钮组
						var count = settings.buttons.length;
						if (count > 0) {
							$.each(settings.buttons, function(k, v) {　　
								var str = idName + "-btn-" + k;
								allBtns.push(str);
								//定义按钮组中的按钮，并放入buttonBox
								$("<p><span id=\"" + str + "\" class=\"green-ui-dialog-button\">" + v.name + "</span></p>").appendTo(buttonBox);
								//为按钮组中的按钮添加click事件
								$("#" + idName).on("click", "#" + str, function() {
									v.callback();
								});
							});
						}
						//把buttonBox加入dialog
						$this.append(buttonBox);

						//为每个按钮添加鼠标移入移出事件
						$.each(allBtns, function(k, v) {
							$("#" + v).mouseover(function() {
								$("#" + v).addClass("green-ui-dialog-button-change");
							}).mouseout(function() {
								$("#" + v).removeClass("green-ui-dialog-button-change");
							});
						});


					}

				};

				//绑定事件到相关对象
				var bindAll = function() {
					//title
					$("#" + idName + "-close").mouseover(function() {
						$("#" + idName + "-close").addClass("green-ui-dialog-close");
					}).mouseout(function() {
						$("#" + idName + "-close").removeClass("green-ui-dialog-close");
					});
					$("#" + idName).on("click", "#box-close", function() {
						if (settings.lock == true) {
							$("#" + idName + "-lock").remove();
						}
						methods.close();
					});

					if (settings.move == true) {
						$("#" + idName + "-titleBox").mouseover(function() {
							$this.css("cursor", "move");
							$("#" + idName + "-titleBox").mousedown(function(event) {
								var offset = $("#" + idName).offset();
								var x1 = event.clientX - offset.left;
								var y1 = event.clientY - offset.top;
								//var btnNum = event.button;
								var btnNum = event.which;
								//$("#" + idName + "-iframeBox").contents().find("p").text(btnNum);
								if (btnNum == 1) {
									$(document).mousemove(function(event) {
										//$("#" + idName + "-iframeBox").contents().find("p").text("x:" + event.clientX + "  y:" + event.clientY);
										$this.css({
											"left": (event.clientX - x1) + "px",
											"top": (event.clientY - y1) + "px",
											"margin": "0"
										});
									});
								}
							}).mouseup(function() {
								$(document).unbind("mousemove");
							});
						}).mouseout(function() {
							$this.css("cursor", "default");
						});

					}

					//button
					$("#" + idName).on("click", "#" + idName + "-btn-ok", function() {
						settings.ok();
					});
					$("#" + idName).on("click", "#" + idName + "-btn-cancel", function() {
						if (settings.lock == true) {
							$("#" + idName + "-lock").remove();
						}
						settings.cancel();
					});



					//content
				};


				//设置Dialog居中显示
				var setDialog = function() {
					var boxWidthAndHeight = autoWidthAndHeight(settings.width, settings.height);
					var boxWidth = boxWidthAndHeight[0];
					var boxHeight = boxWidthAndHeight[1];
					$this.css({
						"left": "50%",
						"top": "50%",
						"margin": "-" + (boxHeight / 2) + "px 0 0 -" + (boxWidth / 2) + "px",
						"width": boxWidth + "px",
						"height": boxHeight + "px",
						"z-index": "10001"
					});
				};

				//自适应宽度和高度
				var autoWidthAndHeight = function(width, height) {
					var boxWidth, boxHeight;
					if (typeof(width) == "number") {
						$this.css("width", width);
						boxWidth = width;
					} else {
						boxWidth = $("#" + idName).width();
					}
					if (typeof(height) == "number") {
						var titleBoxHeight = 0;
						var btnBoxHeight = 0;
						if (typeof(settings.title) == "string") {
							titleBoxHeight = $("#" + idName + "-titleBox").height();
						}
						if (settings.showBtn == true) {
							btnBoxHeight = $("#" + idName + "-buttonBox").height();
						}
						var contentBoxHeight = height - titleBoxHeight - btnBoxHeight - 5;

						$this.css("height", height);
						$("#" + idName + "-contentBox").css("height", contentBoxHeight + "px");
						$("#" + idName + "-buttomBox").css({
							"position": "absolute",
							"bottom": "0px"
						});
						boxHeight = height;
					} else {
						boxHeight = $("#" + idName).height();
					}

					var boxWidthAndHeight = new Array(boxWidth, boxHeight);
					return boxWidthAndHeight;
				};

				drawDialog();
				bindAll();
				setDialog();
				//定义函数结束

			});

		},
		open: function() {
			return this.each(function() {
				var $this = $(this);
				$this.css("display", "inline");
			});
		},
		close: function() {
			//$("#" + idName).empty().css("display", "none");
			$("#" + idName).remove();
		}
	};

})(jQuery);