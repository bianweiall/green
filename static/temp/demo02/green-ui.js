/**
 * $.dialog
 * @extends jquery.1.9.0
 * @fileOverview 弹出层
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2013-12-18
 * Copyright (c) 2013-2013 BW
 * @example
 * $("#box").dialog({
       move: true,
       title: "添加分类",
       buttons: [{
         name: "重置",
         callback: function() {
           alert("重置");
         }
       }, {
         name: "上一步",
         callback: function() {
           alert("上一步");
         }
       }]
   }).dialog("open");
 */
(function($) {
	var idName;
	$.fn.dialog = function() {
		var method = arguments[0];
		if (methods[method]) {
			method = methods[method];
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if (typeof(method) == "object" || !method) {
			method = methods.init;
		} else {
			$.error("Method" + method + "does not exist on jQuery.dialog");
			return this;
		}
		return method.apply(this, arguments);
	};

	var methods = {
		init: function(options) {
			var defaults = {
				move: false,
				lock: false,
				width: "auto",
				height: "auto",
				title: false,
				content: "url:content.html",
				//content: "正在准备内容...正在准备内容...正在准备内容...正在准备内容...",
				showBtn: false,
				okText: "确认",
				cancelText: "取消",
				ok: function() {},
				cancel: function() {
					methods.close();
				},
				buttons: []
			};

			var settings = $.extend({}, defaults, options);

			return this.each(function() {
				var $this = $(this);
				idName = $this.attr("id");
				//var isIframe = settings.content.indexOf('url:');

				var dialogTpl = "<table>" +
					"<tr class=\"green-ui-dialog-title\">" +
					"<td><p></p><span></span></td>" +
					"</tr>" +
					"<tr><td>" +
					"<div class =\"green-ui-dialog-content\">" +
					"<span></span>" +
					"<iframe scrolling=\"no\" frameborder=\"no\"></iframe>" +
					"</div>" +
					"</td></tr>" +
					"<tr><td>" +
					"<div class=\"green-ui-dialog-buttonBox\">" +
					"</div>" +
					"</td></tr>" +
					"</table>";

				var drawDialog = function() {
					$this.append($(dialogTpl));
					$this.addClass("green-ui-dialog");

					if (typeof(settings.title) == "string") {
						dTitle();
					} else {
						$(".green-ui-dialog-title").remove();
					}

					if (settings.showBtn == true) {
						dButton();
					} else {
						$(".green-ui-dialog-buttonBox").remove();
					}


				};

				var dTitle = function() {
					$(".green-ui-dialog-title p").text(settings.title);
					$(".green-ui-dialog-title span").text("X").attr("id", idName + "-close");
					$("#" + idName + "-close").mouseover(function() {
						$("#" + idName + "-close").addClass("green-ui-dialog-title-span-change");
					}).mouseout(function() {
						$("#" + idName + "-close").removeClass("green-ui-dialog-title-span-change");
					});
				};

				var dButton = function() {
					//$(".green-ui-dialog-title h3").text(settings.title);
				};

				drawDialog();

			});

		},
		open: function() {
			return this.each(function() {
				var $this = $(this);
				$this.css("display", "inline");
			});
		},
		close: function() {
			//$("#" + idName).empty().css("display", "none");
			//$("#" + idName).remove();
		}
	};

})(jQuery);