/**
 * $.dialog
 * @extends jquery.1.9.0
 * @fileOverview 弹出层
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2013-02-20
 * Copyright (c) 2013-2013 BW
 * @example
 *    $("#box").dialog().dialog("open");;
 */
(function($) {
	var idName;
	$.fn.dialog = function() {
		var method = arguments[0];
		if (methods[method]) {
			method = methods[method];
			// 我们的方法是作为参数传入的，把它从参数列表中删除，因为调用方法时并不需要它
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if (typeof(method) == "object" || !method) {
			method = methods.init;
		} else {
			$.error("Method" + method + "does not exist on jQuery.dialog");
			return this;
		}
		// 用apply方法来调用我们的方法并传入参数
		return method.apply(this, arguments);
	}

	var methods = {
		init: function(options) {
			var defaults = {
				width: 150,
				height: 150,
				title: false,
				//content: "url:content.html",
				content: "正在准备内容...正在准备内容...",
				showBtn: true,
				okText: "确认",
				cancelText: "取消",
				ok: function() {},
				cancel: function() {
					methods.close();
				},
				buttons: []
			};

			var settings = $.extend({}, defaults, options);
			return this.each(function() {
				var $this = $(this);
				idName = $this.attr("id");

				//标题BOX
				if (typeof(settings.title) == "string") {
					var titleBox = "<div id=\"" + idName + "-titleBox\" class=\"green-ui-dialog-titleBox\"><h3>" + settings.title + "</h3><span><b id=\"" + idName + "-close\">X</b></span></div>";
					$this.append($(titleBox));
					$("#" + idName).on("click", "#box-close", function() {
						methods.close();
					});
					$("#" + idName + "-close").mouseover(function() {
						$("#" + idName + "-close").addClass("green-ui-dialog-close");
					}).mouseout(function() {
						$("#" + idName + "-close").removeClass("green-ui-dialog-close");
					});
				}

				//内容BOX
				var isIframe = settings.content.indexOf("url:");
				var urlStr = settings.content.split("url:")[1];
				var contentBox;
				if (isIframe == -1) {
					contentBox = "<div id=\"" + idName + "-contentBox\" class=\"green-ui-dialog-contentBox\"><p>" + settings.content + "</p></div>";
					$this.append($(contentBox));
				} else {
					contentBox = "<div id=\"" + idName + "-contentBox\" class=\"green-ui-dialog-contentBox\"></div>";
					var contentBoxObj = $(contentBox);
					$("<iframe id=\"" + idName + "-iframeBox\" src=\"" + urlStr + "\"></iframe>").appendTo(contentBoxObj);
					$this.append($(contentBoxObj));
				}

				//按钮BOX
				if (settings.showBtn == true) {
					var buttonBox = $("<div id=\"" + idName + "-buttonBox\" class=\"green-ui-dialog-buttonBox\"></div>");
					var okBtn = idName + "-btn-ok";
					var cancelBtn = idName + "-btn-cancel";
					var allBtns = new Array(okBtn, cancelBtn);
					$("<span><b id=\"" + cancelBtn + "\">" + settings.cancelText + "</b></span>").appendTo(buttonBox);
					$("<span><b id=\"" + okBtn + "\">" + settings.okText + "</b></span>").appendTo(buttonBox);

					//按钮绑定点击事件

					$("#" + idName).on("click", "#" + okBtn, function() {
						settings.ok();
					});
					$("#" + idName).on("click", "#" + cancelBtn, function() {
						settings.cancel();
					});

					var count = settings.buttons.length;
					if (count > 0) {
						$.each(settings.buttons, function(k, v) {　　
							var str = idName + "-btn-" + k;
							allBtns.push(str);
							$("<span><b id=\"" + str + "\">" + v.name + "</b></span>").appendTo(buttonBox);
							$("#" + idName).on("click", "#" + str, function() {
								v.callback();
							});
						});

					}
					$this.append(buttonBox);

					//alert(allBtns.length);
					$.each(allBtns, function(k, v) {　　
						$("#" + v).mouseover(function() {
							$("#" + v).addClass("green-ui-dialog-button-change");
						}).mouseout(function() {
							$("#" + v).removeClass("green-ui-dialog-button-change");
						});
					});

				}

				$this.addClass("green-ui-dialog").css({
					"display": "none",
					"z-index": "10000"
				});


			});

		},
		open: function() {
			return this.each(function() {
				var $this = $(this);
				$this.css("display", "inline");
			});
		},
		close: function() {
			//$("#" + idName).empty().css("display", "none");
			$("#" + idName).remove();
		}
	};

})(jQuery);

/**
 * $.popupDiv
 * @extends jquery.1.9.0
 * @fileOverview 弹出层
 * @author BW
 * @email
 * @site
 * @version 0.1
 * @date 2013-02-20
 * Copyright (c) 2013-2013 BW
 * @example
 *    $("#myDIV").popupDiv();
 */
(function($) {

	$.fn.pop = function(settings, e) {
		var defaultSettings = {
			title: "添加一条记录",
			width: 500,
			height: 400,
			buttons: {}
		};
		var settings = $.extend({}, defaultSettings, settings);

		return this.each(function() {
			popupDiv();
		});

		function popupDiv() {

			var maskDiv = "<div id=\"mask\" class=\"mask\"></div>";

			var popupDiv = "<div id=\"popupDiv\"></div>";
			var popupTitle = "<div id=\"pop-box-title\"><h4>" + settings.title + "</h4></div>";
			var popupContent = "<div id=\"pop-box-body\"></div>";
			var popupButtonPanel = "<div class=\"buttonPanel\" style=\"text-align: right\" style=\"text-align: right\"><a id=\"btn2\">添加</a>||<a id=\"btn1\">取消</a></div>";

			var windowWidth = $(window).width();
			var windowHeight = $(window).height();
			var popupHeight = settings.height;
			var popupWidth = settings.width;

			mask_obj = $(maskDiv);
			$("body").append(mask_obj).append($(popupDiv));
			var div_obj = $("#popupDiv").append($(popupTitle)).append($(popupContent)).append($(popupButtonPanel));

			//加载内容模板
			$("#pop-box-body").empty().load(settings.contentUrl);

			//添加并显示遮罩层
			mask_obj.css({
				"display": "block",
				"position": "absolute",
				"background-color": "gray",
				"left": "0px",
				"top": "0px",
				"width": windowWidth + "px",
				"height": windowHeight + "px",
				"z-index": "10000"
			})
				.fadeIn(200);

			//弹出层
			div_obj.css({
				"position": "absolute",
				"background-color": "white",
				"left": windowWidth / 2 - popupWidth / 2 + "px",
				"top": windowHeight / 2 - popupHeight / 2 + "px",
				"width": popupWidth + "px",
				"height": popupHeight + "px",
				"border": "solid 1px red",
				"z-index": "10001"
			})

			//点击取消按钮
			$("#btn1").click(function() {
				deleteDiv();
			});
			//点击提交按钮
			$("#btn2").click(function() {
				var i
				if ($("#pop_zhuangtai").val() == "入库") {
					i = 0
				} else if ($("#pop_zhuangtai").val() == "出库") {
					i = 1
				}
				var bitem = {
					Caozuoren: $("#pop_caozhuren").val(),
					Shuliang: $("#pop_shuliang").val(),
					Zhuangtai: i,
					Beizhu: $("#pop_beizhu").val(),
					Name: e.Name
				};
				//alert("操作人：" + $("#pop_caozhuren").val() + "数量：" + $("#pop_shuliang").val() + "操作：" + $("#pop_zhuangtai").val() + "备注：" + $("#pop_beizhu").val() + "分类ID：" + e.Id);
				postData(JSON.stringify(bitem));
				deleteDiv();
			});

			//按下鼠标拖动弹出层
			$("#pop-box-title").mousedown(function(e) {
				//改变鼠标指针的形状
				$(this).css("cursor", "move");
				//DIV在页面的位置
				var offset = $(this).offset();
				//获得鼠标指针离DIV元素左边界的距离
				var x = e.pageX - offset.left;
				//获得鼠标指针离DIV元素上边界的距离
				var y = e.pageY - offset.top;
				//绑定鼠标的移动事件，因为光标在DIV元素外面也要有效果，所以要用doucment的事件，而不用DIV元素的事件
				$(document).bind("mousemove", function(ev) {
					//停止当前动画效果
					div_obj.stop();
					//获得X轴方向移动的值
					var _x = ev.pageX - x;
					//获得Y轴方向移动的值
					var _y = ev.pageY - y;

					//移动层
					div_obj.animate({
						left: _x + "px",
						top: _y + "px"
					}, 10);
				});

			});
			//松开鼠标
			$(document).mouseup(function() {
				//改变鼠标指针的形状
				div_obj.css("cursor", "default");
				//移出mousemove事件
				$(this).unbind("mousemove");
			});
		}

		//删除层

		function deleteDiv() {
			$("#mask").remove();
			$("#popupDiv").remove();
		}

	}

})(jQuery);